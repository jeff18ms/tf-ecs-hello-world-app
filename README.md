# Terraform ECS deployment with nodejs hello world app

## provider.tf
- terraform aws provider for static credentials/region of aws account

## ecs-vpc.tf
- vpc resources:
 - 4 subnets; 2 public and 2 private subnets, each AZ will have one of each
 - data block to query available AZ on AWS account/region
 - nat gateway resource will allow resources to communicate within VPC
 - internet gateway resource to allow resources to communicate between vpc and internet

## ecs-alb.tf
 - Internet facing LB will be added to each public subnet with LB security group
 - LB have a listener/target group to forward port 80 traffic to ECS
 - LB security group will only allow port 80 traffic from sources anywhere. 

## ecs-task-definition.tf
 - task definition is defined to use fargate(AWS managed ECS host) with specified cpu/memory allocation.  - the image used is a public Docker image (dockerhub) a nodejs app that returns "Hello World!"
 - the Docker container exposes the nodejs app on port 3000
 - network mode is set to "awsvpc", which will have an elastic network interface and a private IP address  assigned to the task when it runs

## ecs-cluster.tf
 - cluster resource definition

## ecs-service.tf
 - ECS service specifies how many tasks of the application should be run with the task_definition and desired_count properties within the cluster
 - ECS service shouldn't be created until the load balancer has been created - dependency using (depends_on)  
 - security group is created for ecs service, to allow only traffic from load balancer over tcp port 3000 

## Output.tf
 - added output so that it shows the url at end of terraform execution.
